package com.smacvn.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan("com.smacvn.entity")
@EnableJpaRepositories(basePackages = "com.smacvn.repo")
@ComponentScan("com.smacvn")
public class Application {
	public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
