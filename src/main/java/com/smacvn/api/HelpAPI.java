package com.smacvn.api;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.smacvn.dto.HelpDTO;
import com.smacvn.entity.Help;
import com.smacvn.repo.HelpRepo;
import com.smacvn.req.DeleteHelpReq;

@CrossOrigin
@RestController
public class HelpAPI {

	@Autowired
	private HelpRepo repo;

	@PostMapping(value = "/help")
	public Help createHelp(@RequestBody Help h) {
		return repo.save(h);
	}

	@PutMapping(value = "/help")
	public Help updateHelp(@RequestBody Help h) {
		return repo.save(h);
	}

	@DeleteMapping(value = "/help")
	public List<Long> deleteHelp(@RequestBody DeleteHelpReq req) {
		List<Long> lstDeletedId = new ArrayList<>();

		for (Long id : req.getLstId()) {
			Help entityToDelete = repo.findOne(id);

			entityToDelete.setStatus("0");

			lstDeletedId.add(entityToDelete.getHelpId());
			repo.save(entityToDelete);
		}

		return lstDeletedId;
	}

	@GetMapping(value = "/help")
	public List<HelpDTO> getLstHelp() throws Exception {
		return repo.getListHelp();
	}

}
