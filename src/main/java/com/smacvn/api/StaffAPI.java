package com.smacvn.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.smacvn.dto.StaffDTO;
import com.smacvn.repo.StaffRepo;

@CrossOrigin
@RestController
public class StaffAPI {
	
	@Autowired
	private StaffRepo staffRepository;
	
	@GetMapping(value = "/staff")
	public List<StaffDTO> getLstStaff() throws Exception {
		return staffRepository.getListStaff();
	}
	
}
