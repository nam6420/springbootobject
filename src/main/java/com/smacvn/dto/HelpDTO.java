package com.smacvn.dto;

public class HelpDTO {

	private Long staffId;

	private Long parentHelpId;

	private String status;

	private String type;

	private Long shopId;

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public Long getParentHelpId() {
		return parentHelpId;
	}

	public void setParentHelpId(Long parentHelpId) {
		this.parentHelpId = parentHelpId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

}
