package com.smacvn.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "HELP")
public class Help {

	@Id
	@Column(name = "HELP_ID")
	private Long helpId;

	@Column(name = "CONTEXT")
	private String context;

	@Column(name = "HELP_NAME")
	private String helpName;

	@Column(name = "PARENT_HELP_ID")
	private Long parentHelpId;

	@Column(name = "POSITION")
	private String position;

	@Column(name = "STATUS")
	private String status;

	@Column(name = "TYPE")
	private String type;

	@Column(name = "CREATED_USER")
	private String createdUser;

	public Long getHelpId() {
		return helpId;
	}

	public void setHelpId(Long helpId) {
		this.helpId = helpId;
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public String getHelpName() {
		return helpName;
	}

	public void setHelpName(String helpName) {
		this.helpName = helpName;
	}

	public Long getParentHelpId() {
		return parentHelpId;
	}

	public void setParentHelpId(Long parentHelpId) {
		this.parentHelpId = parentHelpId;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

}
