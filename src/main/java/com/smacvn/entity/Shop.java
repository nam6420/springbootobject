package com.smacvn.entity;

import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SHOP")
public class Shop {
	
	@Id
	@Column(name = "SHOP_ID")
	private Long shopId;

	@Column(name = "SHOP_CODE")
	private String shopCode;

	@Column(name = "SHOP_NAME")
	private String shopName;

	@Column(name = "TEL")
	private String tel;

	@Column(name = "ADDRESS")
	private String address;

	@Column(name = "SHOP_PATH")
	private String shopPath;

	@Column(name = "STATUS")
	private String status;

	@Column(name = "CREATE_DATE")
	private Date createDate;

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getShopPath() {
		return shopPath;
	}

	public void setShopPath(String shopPath) {
		this.shopPath = shopPath;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

}
