package com.smacvn.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.smacvn.entity.Help;

@Repository
public interface HelpRepo extends JpaRepository<Help, Long>, HelpRepoCustom {
	
}
