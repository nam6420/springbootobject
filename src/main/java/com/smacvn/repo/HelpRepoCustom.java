package com.smacvn.repo;

import java.util.List;

import com.smacvn.dto.HelpDTO;

public interface HelpRepoCustom {
	
	List<HelpDTO> getListHelp() throws Exception;
	
}
