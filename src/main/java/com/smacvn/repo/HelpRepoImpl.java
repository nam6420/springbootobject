package com.smacvn.repo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;

import com.smacvn.dto.HelpDTO;

public class HelpRepoImpl implements HelpRepoCustom {
	
	@Autowired
	private DataSource dataSourch;

	@Override
	public List<HelpDTO> getListHelp() throws Exception {
		PreparedStatement ps = null;
		Connection connect = null;
		
		List<HelpDTO> list = new ArrayList<>();
		
		try {
			
			connect = dataSourch.getConnection();
			
			String sql = "SELECT s.STAFF_ID, h.PARENT_HELP_ID, h.STATUS, h.TYPE,s.SHOP_ID from STAFF s , HELP h WHERE h.CREATED_USER = s.STAFF_NAME";
			ps = connect.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) {
				HelpDTO dto = new HelpDTO();
				dto.setStaffId(rs.getLong("STAFF_ID"));
				dto.setParentHelpId(rs.getLong("PARENT_HELP_ID"));
				dto.setStatus(rs.getString("STATUS"));
				dto.setType(rs.getString("TYPE"));
				dto.setShopId(rs.getLong("SHOP_ID"));
				list.add(dto);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(ps != null) {
				ps.close();
			}
			if(connect != null) {
				connect.close();
			}
		}
		return list;
	}
	
}
