package com.smacvn.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.smacvn.entity.Shop;

@Repository
public interface ShopRepo extends JpaRepository<Shop, Long>{
	
}
