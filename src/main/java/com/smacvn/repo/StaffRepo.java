package com.smacvn.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.smacvn.entity.Staff;

@Repository
public interface StaffRepo extends JpaRepository<Staff, Long>, StaffRepoCustom {

}
