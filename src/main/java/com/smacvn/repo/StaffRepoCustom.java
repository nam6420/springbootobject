package com.smacvn.repo;

import java.util.List;

import com.smacvn.dto.StaffDTO;

public interface StaffRepoCustom {
	
	List<StaffDTO> getListStaff() throws Exception;
	
}
