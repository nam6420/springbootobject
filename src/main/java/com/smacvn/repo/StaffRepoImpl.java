package com.smacvn.repo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;

import com.smacvn.dto.StaffDTO;

public class StaffRepoImpl implements StaffRepoCustom {

	@Autowired
	private DataSource dataSourch;

	@Override
	public List<StaffDTO> getListStaff() throws Exception {
		PreparedStatement ps = null;
		Connection connect = null;

		List<StaffDTO> list = new ArrayList<>();

		try {

			connect = dataSourch.getConnection();

			String sql = "SELECT s.STAFF_ID, s.STAFF_NAME, s.TEL, s.STATUS,s.SHOP_ID, h.HELP_ID, s.ID_NO FROM STAFF s , HELP h WHERE h.CREATED_USER = s.STAFF_NAME";
			ps = connect.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				StaffDTO dto = new StaffDTO();
				dto.setStaffId(rs.getLong("STAFF_ID"));
				dto.setStaffName(rs.getString("STAFF_NAME"));
				dto.setTel(rs.getString("TEL"));
				dto.setStatus(rs.getString("STATUS"));
				dto.setShopId(rs.getLong("SHOP_ID"));
				dto.setHelpId(rs.getLong("HELP_ID"));
				dto.setIdNo(rs.getString("ID_NO"));
				list.add(dto);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (ps != null) {
				ps.close();
			}
			if (connect != null) {
				connect.close();
			}
		}
		return list;
	}

}
