package com.smacvn.req;

import java.util.List;

public class DeleteHelpReq {

	private List<Long> lstId;

	public List<Long> getLstId() {
		return lstId;
	}

	public void setLstId(List<Long> lstId) {
		this.lstId = lstId;
	}

}
